package model.report;

import javafx.scene.text.Font;
import javafx.scene.text.Text;

/**
 * Created by Daniel on 08/04/2016.
 *
 */
public class ReportSection extends Text {

    //Report settings variables
    Text heading, subheading, body;

    public ReportSection(String heading, String subheading, String body) {
        this.heading = new Text(heading);
        this.body = new Text(body);
        this.subheading = new Text(subheading);
        setReportSettings();
    }

    /**
     * Sets the text font and colour size of all the headings, etc.
     */
    public void setReportSettings() {
        heading.setFont(Font.font("Verdana", 20));
        subheading.setFont(Font.font("Verdana", 18));

        body.setFont(Font.font(12));
    }

    /*
     ************************************** GETTERS AND SETTERS */

    public String getHeadingText() {
        return heading.getText();
    }

    public void setHeadingText(String heading) {
        this.heading = new Text(heading);
    }

    public String getSubheadingText() {
        return subheading.getText();
    }

    public void setSubheadingText(String subheading) {
        this.subheading = new Text(subheading);
    }

    public String getBodyText() {
        return body.getText();
    }

    public void setBody(String body) {
        this.body = new Text(body);
    }

    public Text getHeading() {
        return heading;
    }

    public void setHeading(Text heading) {
        this.heading = heading;
    }

    public Text getSubheading() {
        return subheading;
    }

    public void setSubheading(Text subheading) {
        this.subheading = subheading;
    }

    public Text getBody() {
        return body;
    }

    public void setBody(Text body) {
        this.body = body;
    }
}
