package model.report;

import javafx.scene.text.Text;
import main.Main;
import model.data.Department;
import model.data.Staff;
import model.data.enums.JobProblemType;
import model.workingmemory.TempMemory;
import utils.Utilities;

import java.util.ArrayList;

/**
 * Created by Daniel on 20/04/2016.
 *
 */
public class ReportInterface {

    ArrayList<ReportSection> reportSections = new ArrayList<>();

    public void generateJobReport(Main application, JobProblemType jobProblemType) {

        //  SECTION 1
        System.out.println("Getting the body elements from local memory");
        //  Get the body elements from local memory
        int total = application.getTempMemory().getTechnicalErrorCount() + application.getTempMemory().getTraningCount();
        float techper = Utilities.percentageCalc(total, application.getTempMemory().getTechnicalErrorCount());
        float trainper = Utilities.percentageCalc(total, application.getTempMemory().getTraningCount());

        Text title    = new Text("Common Jobs");
        Text subtitle = new Text("Most common job problem type by %");

        String bodyCommonProbType = String.format("The most common problem type is: " + jobProblemType + "\n\n" +
                "Break down: \n\n" +
                "Technical Problems - %.2f%% \n" +
                "Training Problems - %.2f%% \n\n", techper, trainper);

        Text body = new Text(bodyCommonProbType);
        System.out.println("Section created");


        //  SECTION 2
        Text title2 = new Text("Job Prevention");
        Text subTitle2 = new Text("Prevention through training");
        String bodyText = String.format("The amount of jobs that can be solved through training is:\n\n%.2f%%\n\n", trainper) +
                 bodyText(application) + "\n\nThese jobs were marked as caused by a lack of training in the job area. These jobs can be avoided" +
                "if more time was spent on training the users on how to use the application in question. All other jobs were caused by a variety of" +
                "errors that could find a suitable conclusion as to how they can be avoided.\n\n";

        Text body2 = new Text(bodyText);

        //  SECTION 3
        Text title3 = new Text("Job Solutions");
        Text subTitle3 = new Text("Jobs unsolved with solution in database:");

        String bodyText3 = "Job in need of solution and its solution, respectively: \n\n" + bodyText3(application) +
                "\nThese jobs were marked as uncomplete in the job queue. The queue was checked for the same error and " +
                "compared against the job. The job that has the same problem and a working solution was shown.\n\n";
        Text body3 = new Text(bodyText3);

        reportSections.add(new ReportSection(title.getText(), subtitle.getText(), body.getText()));
        reportSections.add(new ReportSection(title2.getText(), subTitle2.getText(), body2.getText()));
        reportSections.add(new ReportSection(title3.getText(), subTitle3.getText(), body3.getText()));
        Report report = new Report();

        for (int i = 0; i < reportSections.size(); i++) {
            System.out.println("Adding sections");
            report.add(reportSections.get(i));
            System.out.println("sections added3");
        }

        application.getTempMemory().setReport(report);

    }

    private String bodyText(Main application) {
        StringBuilder stringBuild = new StringBuilder();
        ArrayList<String> jobString = new ArrayList<>();

        TempMemory tempmemory = application.getTempMemory();
        for (int i = 0; i < tempmemory.getJobList().size(); i++) {
            application.getTempMemory().getJobList();

            if (tempmemory.getJobList().get(i).getJobProblemType() == JobProblemType.TRAINING) {
                jobString.add(tempmemory.getJobList().get(i).getJobName());
            }
        }
        stringBuild.append("\nThe following jobs are:\n\n");
        for (String i : jobString) {
            stringBuild.append(i + "\n");
            System.out.println(stringBuild);
        }


        return String.valueOf(stringBuild);
    }

    private String bodyText3(Main application) {
        StringBuilder stringBuilder = new StringBuilder();

        TempMemory tempMemory = application.getTempMemory();
        for (int i = 0; i < tempMemory.getJobsolutions().size(); i++) {
            stringBuilder.append(tempMemory.getJobsolutions().get(i).getNeedSolution().getJobName() + ", " +
                    tempMemory.getJobsolutions().get(i).getNeedSolution().getJobName() + "\n");
        }
        stringBuilder.append("\n");

        return String.valueOf(stringBuilder);
    }

    public void generateStaffReport(Main application, Staff staff) {
        TempMemory tempMemory = application.getTempMemory();

        //  SECTION 1
        Text title = new Text("Most Calls");
        Text subtitle = new Text("Staff with the most calls to the deparement");

        String bodyText = "The user with the most calls made to the department is:\n" + tempMemory.getHighestCaller().getStaffName() +
                "\nFrom: " + tempMemory.getHighestCaller().getDepartmentType() + "\n\nIt is most likely that this user " +
                "takes up the most time from the IT department. Check his/her jobs is recommended to find solution to" +
                " reduce the amount of calls from that user\n\n";

        Text body = new Text(bodyText);

        //  SECTION 2
        Text title2 = new Text("Staff Calls Breakdown");
        Text subtitle2 = new Text("Staff with jobs that can be avoided through training\n");

        String bodyText2 = staff.getStaffName() + " is the staff member that has been identified with the most jobs that" +
                " can be avoided through training.\n\n"
                + "The jobs are as following: \n";

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(bodyText2);

        for (int i = 0; i < staff.getJobs().size(); i++) {
            stringBuilder.append(staff.getJobs().get(i).getJobName() + "\n");
        }

        stringBuilder.append("\nThe user's most common problem is with the jobs listed previously. By training the user" +
                ", then there is a high possibility of less calls being made to the IT department by this user.");


        Text body2 = new Text(String.valueOf(stringBuilder));


        //  ADDING THE TEXT TO THE SECTIONS
        reportSections.add(new ReportSection(title.getText(), subtitle.getText(), body.getText()));
        reportSections.add(new ReportSection(title2.getText(), subtitle2.getText(), body2.getText()));

        //  ADD THE SECTIONS TO THE REPORT
        Report report = new Report();
        for (ReportSection r : reportSections) {
            report.add(r);
        }

        application.getTempMemory().setReport(report);

    }

    public void generateDepartmentReport(Main application) {
        TempMemory tempMemory = application.getTempMemory();

        //  SECTION 1
        Text title1 = new Text("Department Breakdown");
        Text subtitle1 = new Text("Department calls by %");

        // Get the total calls of all the departments
        int count = 0;
        for (int i = 0; i < tempMemory.getDepartmentList().size(); i++) {
            count  = count + tempMemory.getDepartmentList().get(i).getStaffTotalCalls();
        }

        // Find the percentage of each
        ArrayList<Float> percentages = new ArrayList<>();
        ArrayList<Department> dept = new ArrayList<>();
        for (int i = 0; i < tempMemory.getDepartmentList().size(); i++) {
            // int percentage -- get the department to check and run the total through the util function
            float percentage = Utilities.percentageCalc(count, tempMemory.getDepartmentList().get(i).getStaffTotalCalls());
            dept.add(tempMemory.getDepartmentList().get(i));
        }

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Breakdown of each department:\n");

        for (int i = 0; i < percentages.size(); i++)
            stringBuilder.append(String.format(dept.get(i) + "  %.2f%%", percentages.get(i)));

        Text body1 = new Text(String.valueOf(stringBuilder));

        //  SECTION 2
        Text title2 = new Text("User Calls");
        Text subtitle2 = new Text("User with most calls from the department");

        String bodyText2 = "";

        Text body2 = new Text(bodyText2);

        //  ADD ALL TO SECTION REPORTS
        reportSections.add(new ReportSection(title1.getText(), subtitle1.getText(), body1.getText()));
        reportSections.add(new ReportSection(title2.getText(), subtitle2.getText(), body2.getText()));

        //  ADD ALL SECTIONS TO THE REPORT
        Report report = new Report();
        for (ReportSection r : reportSections)
            report.add(r);

        tempMemory.setReport(report);
    }

}
