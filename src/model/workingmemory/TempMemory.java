package model.workingmemory;

import model.data.Department;
import model.data.Jobs;
import model.data.Staff;
import model.data.enums.DepartmentType;
import model.data.enums.JobProblemType;
import model.data.enums.JobSectionType;
import model.report.Report;

import java.util.ArrayList;
import java.util.jar.Pack200;

/**
 * Created by Daniel on 06/04/2016.
 *
 */
public class TempMemory {

    /* Misc Arrays */
    private ArrayList<String> recentDocuments = new ArrayList<String>(); //TODO
    private ArrayList searchParameterList = new ArrayList<String>(); //type may change
    private ArrayList<String> keyWords = new ArrayList<String>();

    private ArrayList<Jobs> jobList = new ArrayList<>();
    private ArrayList<Jobs> completedJobs = new ArrayList<>();
    private ArrayList<Jobs> uncompletedJobs = new ArrayList<>();
    private ArrayList<ConnectedList> jobsolutions = new ArrayList<>();

    private ArrayList<Staff> staffList = new ArrayList<>();
    private ArrayList<ConnectedList> staffNeedTraining = new ArrayList<>();

    private ArrayList<Department> departmentList = new ArrayList<>();

    private int technicalErrorCount;
    private int traningCount;
    private int callCount = 0;
    private Staff highestCaller;
    private Department mostCallsDepartment;


    private String fileName;
    private Report report;


    /*
     ****************************************************** Getters and setters
     */
    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Report getReport() {
        return report;
    }

    public void setReport(Report report) {
        this.report = report;
    }

    public void addKeyWordsToArray (String word) {
        keyWords.add(word);
    }

    public ArrayList getKeyWordsArray() {
        return keyWords;
    }

    public ArrayList getRecentDocuments() {
        return recentDocuments;
    }

    public void setRecentDocuments(ArrayList<String> recentDocuments) {
        this.recentDocuments = recentDocuments;
    }

    public ArrayList getSearchParameterList() {
        return searchParameterList;
    }

    public void setSearchParameterList(ArrayList searchParameterList) {
        this.searchParameterList = searchParameterList;
    }

    public ArrayList<String> getKeyWords() {
        return keyWords;
    }

    public void setKeyWords(ArrayList<String> keyWords) {
        this.keyWords = keyWords;
    }

    public ArrayList<Jobs> getJobList() {
        return jobList;
    }

    public void setJobList(ArrayList<Jobs> jobList) {
        this.jobList = jobList;
    }

    public ArrayList<Jobs> getCompletedJobs() {
        return completedJobs;
    }

    public void setCompletedJobs(ArrayList<Jobs> completedJobs) {
        this.completedJobs = completedJobs;
    }

    public ArrayList<Jobs> getUncompletedJobs() {
        return uncompletedJobs;
    }

    public void setUncompletedJobs(ArrayList<Jobs> uncompletedJobs) {
        this.uncompletedJobs = uncompletedJobs;
    }

    public ArrayList<ConnectedList> getJobsolutions() {
        return jobsolutions;
    }

    public void setJobsolutions(ArrayList<ConnectedList> jobsolutions) {
        this.jobsolutions = jobsolutions;
    }

    public int getTechnicalErrorCount() {
        return technicalErrorCount;
    }

    public void setTechnicalErrorCount(int technicalErrorCount) {
        this.technicalErrorCount = technicalErrorCount;
    }

    public int getTraningCount() {
        return traningCount;
    }

    public void setTraningCount(int traningCount) {
        this.traningCount = traningCount;
    }

    public ArrayList<Staff> getStaffList() {
        return staffList;
    }

    public void setStaffList(ArrayList<Staff> staffList) {
        this.staffList = staffList;
    }

    public int getCallCount() {
        return callCount;
    }

    public void setCallCount(int callCount) {
        this.callCount = callCount;
    }

    public Staff getHighestCaller() {
        return highestCaller;
    }

    public void setHighestCaller(Staff highestCaller) {
        this.highestCaller = highestCaller;
    }

    public ArrayList<ConnectedList> getStaffNeedTraining() {
        return staffNeedTraining;
    }

    public void setStaffNeedTraining(ArrayList<ConnectedList> staffNeedTraining) {
        this.staffNeedTraining = staffNeedTraining;
    }

    public ArrayList<Department> getDepartmentList() {
        return departmentList;
    }

    public void setDepartmentList(ArrayList<Department> departmentList) {
        this.departmentList = departmentList;
    }

    public Department getMostCallsDepartment() {
        return mostCallsDepartment;
    }

    public void setMostCallsDepartment(Department mostCallsDepartment) {
        this.mostCallsDepartment = mostCallsDepartment;
    }

    //------------------------------------ TEST METHOD
    public void generateTests() {
        //  Job test sets
        jobList.add(new Jobs(true, 1, "Errors with microsoft word", JobSectionType.MICROSOFT_APPLICATION, DepartmentType.ADVERTISMENT, JobProblemType.TRAINING, "Training needed"));
        jobList.add(new Jobs(true, 2, "Can't find where function is", JobSectionType.MICROSOFT_APPLICATION, DepartmentType.ADVERTISMENT, JobProblemType.TRAINING, "Training needed"));
        jobList.add(new Jobs(true, 3, "Lost work due to error", JobSectionType.MICROSOFT_APPLICATION, DepartmentType.ADVERTISMENT, JobProblemType.TRAINING, "Training needed"));
        jobList.add(new Jobs(true, 4, "Lost work due to error", JobSectionType.MICROSOFT_APPLICATION, DepartmentType.ADVERTISMENT, JobProblemType.TRAINING, "Training needed"));
        jobList.add(new Jobs(true, 5, "crashed application", JobSectionType.MICROSOFT_APPLICATION, DepartmentType.ADVERTISMENT, JobProblemType.TECHNICAL_ERROR, "Technical error, turned off and on and it worked."));
        jobList.add(new Jobs(true, 6, "crashed application", JobSectionType.MICROSOFT_APPLICATION, DepartmentType.ADVERTISMENT, JobProblemType.TECHNICAL_ERROR, "Technical error, revoked access and then re-added. Crashes stopped soon after"));

        jobList.add(new Jobs(false, 7, "Errors with microsoft word", JobSectionType.MICROSOFT_APPLICATION, DepartmentType.ADVERTISMENT, JobProblemType.TRAINING));
        jobList.add(new Jobs(false, 8, "Can't find where function is", JobSectionType.MICROSOFT_APPLICATION, DepartmentType.ADVERTISMENT));
        jobList.add(new Jobs(false, 9, "Lost work due to error", JobSectionType.MICROSOFT_APPLICATION, DepartmentType.ADVERTISMENT));
        jobList.add(new Jobs(false, 10, "Lost work due to error", JobSectionType.MICROSOFT_APPLICATION, DepartmentType.ADVERTISMENT));
        jobList.add(new Jobs(false, 11, "crashed application", JobSectionType.MICROSOFT_APPLICATION, DepartmentType.ADVERTISMENT));
        jobList.add(new Jobs(false, 12, "crashed application", JobSectionType.MICROSOFT_APPLICATION, DepartmentType.ADVERTISMENT));

        //  staff test sets
        staffList.add(new Staff(1, "Sam Liddle", DepartmentType.LEGAL, 78));
        staffList.add(new Staff(2, "Garry Lydon", DepartmentType.LEGAL,89));
        staffList.add(new Staff(3, "Amy Leslie", DepartmentType.LEGAL, 34));
        staffList.add(new Staff(4, "Mathew Henderson", DepartmentType.LEGAL,12));
        staffList.add(new Staff(5, "Anthony Shepherd", DepartmentType.LEGAL, 57));
        staffList.add(new Staff(6, "Craig Liddle", DepartmentType.GENIE, 43));
        staffList.add(new Staff(7, "Rebecca Davidson", DepartmentType.GENIE, 89));
        staffList.add(new Staff(8, "Jake Gowland", DepartmentType.GENIE, 99));
        staffList.add(new Staff(9, "Julie Liddle", DepartmentType.GENIE, 101));
        staffList.add(new Staff(10, "Graham Liddle", DepartmentType.GENIE, 50));

        staffList.add(new Staff(11, "Matthew Brady", DepartmentType.HOMES,43));
        staffList.add(new Staff(12, "Sophie Karter", DepartmentType.HOMES, 34));
        staffList.add(new Staff(13, "Julian Lescott", DepartmentType.HOMES, 23));
        staffList.add(new Staff(14, "Alex Song", DepartmentType.HOMES, 87));
        staffList.add(new Staff(15, "Sarah Daniels", DepartmentType.HOMES, 56));
        staffList.add(new Staff(16, "Lillian Kane", DepartmentType.HOMES, 87));
        staffList.add(new Staff(17, "Barry B. Barker", DepartmentType.HOMES, 93));
        staffList.add(new Staff(18, "Wayne Potts", DepartmentType.EXECUTIVES, 73));
        staffList.add(new Staff(19, "Julie Hutchinson", DepartmentType.EXECUTIVES, 10));
        staffList.add(new Staff(20, "Hank Tucker", DepartmentType.EXECUTIVES, 130));

        //  Add jobs to staff members
        int count = 0;
        for (int i = 0; i< staffList.size(); i ++) {
            if (count < 10)
                for (int j = 0; j < 10; j++)
                    count ++;
                    staffList.get(i).getJobs().add(new Jobs(true, 1, "Errors with microsoft word", JobSectionType.MICROSOFT_APPLICATION, DepartmentType.ADVERTISMENT, JobProblemType.TRAINING, "Training needed"));
            if (count < 20)
                for (int j = 0; j < 5; j++)
                    count ++;
                    staffList.get(i).getJobs().add(new Jobs(true, 1, "Errors with microsoft word", JobSectionType.MOBILE, DepartmentType.ADVERTISMENT, JobProblemType.TRAINING, "Training needed"));
            if (count < 25)
                for (int j = 0; j < 23; j ++)
                    staffList.get(i).getJobs().add(new Jobs(true, 1, "Errors with microsoft word", JobSectionType.HARDWARE, DepartmentType.ADVERTISMENT, JobProblemType.TECHNICAL_ERROR, "Technical Error"));
            if (count < 48)
                for (int j = 0; j < 10; j++)
                    staffList.get(i).getJobs().add(new Jobs(true, 1, "Errors with microsoft word", JobSectionType.MOBILE, DepartmentType.ADVERTISMENT, JobProblemType.TECHNICAL_ERROR, "Technical Error"));
        }


        //  Create department test sets
        departmentList.add(new Department(DepartmentType.LEGAL));
        departmentList.add(new Department(DepartmentType.GENIE));
        departmentList.add(new Department(DepartmentType.HOMES));
        departmentList.add(new Department(DepartmentType.EXECUTIVES));

        count = 0;
        for (int j = 0; j < departmentList.size(); j++) {
            if (count < 5) // legal
                for (int i = 0; i < 5; i++) {
                    count ++;
                    departmentList.get(j).getStaffArray().add(staffList.get(j));
                }
            if (count < 10) // genie
                for (int i = 0; i < 10; i++) {
                    count++;
                    departmentList.get(j).getStaffArray().add(staffList.get(j));
                }
            if (count < 17) // homes
                for (int i = 0; i < 17; i++) {
                    count ++;
                    departmentList.get(j).getStaffArray().add(staffList.get(j));
                }
            if (count < 20) //Execs.
                for (int i = 0; i < 20; i++) {
                    count++;
                    departmentList.get(j).getStaffArray().add(staffList.get(j));
                }
        }




        for (Jobs job : jobList) {
            if (job.isCompleted())  completedJobs.add(job);
            if (!job.isCompleted()) uncompletedJobs.add(job);
        }

        System.out.println("Completed jobs: " + completedJobs.size());
        System.out.println("uncompleted jobs: " + uncompletedJobs.size());
        System.out.println("Staff list size: " + staffList.size());
        System.out.println("\n\n");
    }
}
