package model.workingmemory;

import model.data.Jobs;
import model.data.Staff;
import model.data.enums.JobProblemType;
import model.data.enums.JobSectionType;

import java.util.ArrayList;

/**
 * Created by Daniel on 18/04/2016.
 *
 */
public class ConnectedList {

    Jobs needSolution;
    Jobs solution;
    Staff needTraining;
    JobSectionType jobSectionType;

    public ConnectedList(Jobs needSolution, Jobs solution) {
        this.needSolution = needSolution;
        this.solution = solution;
    }

    public ConnectedList(Staff needTraining, JobSectionType jobSectionType) {
        this.needTraining = needTraining;
        this.jobSectionType = jobSectionType;
    }

    //---------------------------------------- GETTERS AND SETTERS

    public Jobs getNeedSolution() {
        return needSolution;
    }

    public void setNeedSolution(Jobs needSolution) {
        this.needSolution = needSolution;
    }

    public Jobs getSolution() {
        return solution;
    }

    public void setSolution(Jobs solution) {
        this.solution = solution;
    }

    public Staff getNeedTraining() {
        return needTraining;
    }

    public void setNeedTraining(Staff needTraining) {
        this.needTraining = needTraining;
    }

    public JobSectionType getJobSectionType() {
        return jobSectionType;
    }

    public void setJobSectionType(JobSectionType jobSectionType) {
        this.jobSectionType = jobSectionType;
    }
}
