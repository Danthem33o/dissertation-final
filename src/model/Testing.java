package model;

import java.util.ArrayList;

/**
 * Created by Daniel on 10/03/2016.
 *
 * Class testing the tutorial page. Its intended use is only or demonstration purposes.
 */
public class Testing {

    public static ArrayList<String> testKeyWords = new ArrayList<>();

    public static String tutorialBodyText = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris id ipsum et elit cursus tempor nec eget nisi. Suspendisse rhoncus id odio nec gravida. Quisque quis nisi tellus. Integer fringilla magna eu nulla ullamcorper, ut lacinia tortor molestie. Donec fringilla nec neque commodo pulvinar. Sed euismod eu leo in vulputate. Praesent consequat, felis eu tincidunt fringilla, erat turpis varius ex, in euismod sem risus ac orci. In euismod venenatis porta. Aliquam pharetra ex ut dui tristique luctus. Maecenas elementum magna egestas, ullamcorper est at, vehicula arcu. Mauris iaculis consequat convallis. Cras a vulputate diam.\n" +
            "\n" +
            "Proin vitae sapien nisl. Curabitur efficitur ante id semper commodo. Donec at enim aliquam, efficitur tellus vitae, venenatis sem. Fusce consequat hendrerit diam sit amet semper. Donec ut enim eros. Praesent maximus mollis interdum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;";


    public static String tutorialBodyText2 = "Shorter body here, with different text. "
            + "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris id ipsum et elit cursus tempor nec eget nisi. Suspendisse rhoncus id odio nec gravida. Quisque quis nisi tellus. Integer fringilla magna eu nulla ullamcorper, ut lacinia tortor molestie. Donec fringilla nec neque commodo pulvinar. Sed euismod eu leo in vulputate. Praesent consequat, felis eu tincidunt fringilla, erat turpis varius ex, in euismod sem risus ac orci.";
}


