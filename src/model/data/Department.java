package model.data;

import model.data.enums.DepartmentType;

import java.util.ArrayList;

/**
 * Created by Daniel on 15/04/2016.
 *
 * This class was created to test the expert system.
 */
public class Department {

    private int departmentID;
    private DepartmentType departmentType;
    private ArrayList<Staff> staff = new ArrayList<>();

    public Department() {
    }

    public Department(DepartmentType departmentType) {
        this.departmentType = departmentType;
    }

    //------------------------------------- GETTERS AND SETTERS

    public Staff getStaffFromArray(int i) {
        return staff.get(i);
    }

    public void addStaffToArray(Staff s) {
        staff.add(s);
    }

    public ArrayList<Staff> getStaffArray() {
        return staff;
    }

    public void setStaff(ArrayList<Staff> staff) {
        this.staff = staff;
    }

    public DepartmentType getDepartmentType() {
        return departmentType;
    }

    public void setDepartmentType(DepartmentType departmentType) {
        this.departmentType = departmentType;
    }

    public int getStaffTotalCalls() {
        int count = 0;

        for (int i = 0; i < staff.size(); i++) {
            int staffCount = staff.get(i).getJobCount();
            count = staffCount + count;
        }

        return count;
    }
}
