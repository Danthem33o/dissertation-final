package model.data;

import model.data.enums.DepartmentType;
import model.data.enums.JobSectionType;

import java.util.ArrayList;

/**
 * Created by Daniel on 15/04/2016.
 *
 * This class was created to test the expert system.
 */
public class Staff {

    private String staffName;
    private int staff_id;

    private Department department;
    private DepartmentType departmentType;
    private ArrayList<Jobs> jobs = new ArrayList<>();

    private int jobCount;

    public Staff() {

    }

    public Staff(int staff_id, String staffName, DepartmentType departmentType, int jobCount) {
        this.staff_id = staff_id;
        this.staffName = staffName;
        this.departmentType = departmentType;
        this.jobCount = jobCount;
    }

    public void createJobs() {

        int count = 0;
        for (int i = 0; i < 10; i++) {
            count++;
            //jobs.add(new Jobs(true, count, "problem" + count, JobSectionType.MOBILE));
        }

    }

    public String getStaffName() {
        return staffName;
    }

    public void setStaffName(String staffName) {
        this.staffName = staffName;
    }

    public int getStaff_id() {
        return staff_id;
    }

    public void setStaff_id(int staff_id) {
        this.staff_id = staff_id;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public ArrayList<Jobs> getJobs() {
        return jobs;
    }

    public void setJobs(ArrayList<Jobs> jobs) {
        this.jobs = jobs;
    }

    public int getJobCount() {
        return jobCount;
    }

    public void setJobCount(int jobCount) {
        this.jobCount = jobCount;
    }

    public DepartmentType getDepartmentType() {
        return departmentType;
    }

    public void setDepartmentType(DepartmentType departmentType) {
        this.departmentType = departmentType;
    }

}
