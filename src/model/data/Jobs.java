package model.data;

import model.data.enums.DepartmentType;
import model.data.enums.JobProblemType;
import model.data.enums.JobSectionType;

/**
 * Created by Daniel on 15/04/2016.
 *
 * This job was created to test the expert system.
 */
public class Jobs {

    private boolean completed;
    private boolean needSolution = false;
    private int job_id;
    private String jobName;
    private String solution;
    private JobSectionType jobSection;
    private JobProblemType jobProblemType;
    private DepartmentType departmentType;


    public Jobs() {
    }

    public Jobs(boolean completed, int job_id, String jobName, JobSectionType jobSection, DepartmentType departmentType,
                JobProblemType jobProblemType, String solution) {
        this.completed = completed;
        this.job_id = job_id;
        this.jobName = jobName;
        this.jobSection = jobSection;
        this.departmentType = departmentType;
        this.jobProblemType = jobProblemType;
        this.solution = solution;
    }

    public Jobs(boolean completed, int job_id, String jobName, JobSectionType jobSection, DepartmentType departmentType, JobProblemType jobProblemType) {
        this.completed = completed;
        this.job_id = job_id;
        this.jobName = jobName;
        this.jobSection = jobSection;
        this.departmentType = departmentType;
        this.jobProblemType = jobProblemType;
    }

    public Jobs(boolean completed, int job_id, String jobName, JobSectionType jobSection, DepartmentType departmentType) {
        this.completed = completed;
        this.job_id = job_id;
        this.jobName = jobName;
        this.jobSection = jobSection;
        this.departmentType = departmentType;
    }


    //-------------------------------------- GETTERS AND SETTERS
    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public int getJob_id() {
        return job_id;
    }

    public void setJob_id(int job_id) {
        this.job_id = job_id;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public JobSectionType getJobSection() {
        return jobSection;
    }

    public void setJobSection(JobSectionType jobSection) {
        this.jobSection = jobSection;
    }

    public DepartmentType getDepartmentType() {
        return departmentType;
    }

    public void setDepartmentType(DepartmentType departmentType) {
        this.departmentType = departmentType;
    }

    public boolean isInNeedSolution() {
        return needSolution;
    }

    public void setInNeedSolution(boolean needSolution) {
        this.needSolution = needSolution;
    }

    public String getSolution() {
        return solution;
    }

    public void setSolution(String solution) {
        this.solution = solution;
    }

    public JobProblemType getJobProblemType() {
        return jobProblemType;
    }

    public void setJobProblemType(JobProblemType jobProblemType) {
        this.jobProblemType = jobProblemType;
    }
}
