package model.data.enums;

/**
 * Created by Daniel on 15/04/2016.
 *
 * A job may have a product used in it. These products will fall under different categories.
 *
 * This enum was created to test the expert system.
 */
public enum ProductType {

    TERMINAL,
    COMPUTER,
    LAPTOP,
    PHONE,
    TABLET,
    PERIPHERALS

}
