package model.data.enums;

/**
 * Created by Daniel on 15/04/2016.
 *
 * These are the different types of departments that can be found on a job.
 * The ones below are a sample of what is available.
 *
 * This enum is created to prove and test the expert system.
 */
public enum DepartmentType {

    IT,
    CONSTRUCTION,
    HOMES,
    CALLCENTRE,
    LEGAL,
    GENIE,
    EXECUTIVES,
    ADVERTISMENT

}
