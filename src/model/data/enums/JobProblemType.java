package model.data.enums;

/**
 * Created by Daniel on 18/04/2016.
 *
 * There are many different types of problem types can be found in a job.
 *
 * This enum was created to test the expert system.
 */
public enum JobProblemType {

    TRAINING,
    TECHNICAL_ERROR,

}
