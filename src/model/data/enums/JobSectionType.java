package model.data.enums;

/**
 * Created by Daniel on 15/04/2016.
 *
 * Every job is listed under a job section type.
 * This is a small example of what is available in a job.
 *
 * This enum was created to test the expert system.
 *
 */
public enum JobSectionType {

    MOBILE,
    MICROSOFT_APPLICATION,
    HARDWARE,
    LAPTOP,
    COMPUTER

}
