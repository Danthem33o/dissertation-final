package model;

import java.util.ArrayList;

/**
 * Created by Daniel on 08/03/2016.
 *
 */
public class RecentFilesBuffer {

    private ArrayList<String> recentDocuments = new ArrayList<>(9); //10
    private int count = 0;

    public void addRecentDocument(String document) {
        recentDocuments.add(count, document);
        count ++;

        if (count == 9) {
            count = 0;
        }
    }

    public ArrayList<String> getRecentDocuments() {
        return recentDocuments;
    }

}
