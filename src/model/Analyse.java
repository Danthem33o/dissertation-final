package model;

import main.Main;
import model.data.Department;
import model.data.Staff;
import model.data.enums.JobProblemType;
import model.expertsystem.ExpertSystem;
import model.report.ReportInterface;
import model.workingmemory.TempMemory;

import java.util.ArrayList;
import java.util.Base64;

/**
 * Created by Daniel on 09/04/2016.
 *
 */
public class Analyse {

    Main application;

    public Analyse() {
    }

    public Analyse(Main application) {
        this.application = application;
    }


    public void analyseJobs() {
        System.out.println("Analysing jobs...");
        System.out.println("Job list size:" + application.getTempMemory().getJobList().size());
        int jobListSize = application.getTempMemory().getJobList().size();

        //   Check each against the rules
        for (int i = 0; i < jobListSize; i ++) {
            new ExpertSystem().rules(application.getTempMemory().getJobList().get(i), application.getTempMemory());
        }

        //   Save to the report - get the data from local memory
        getJobProblemTypeTotals();
        new ReportInterface().generateJobReport(application, getMostCommonJobProblemType());
    }

    public void analyseStaff() {
        System.out.println("Analysing Staff..");

        int staffListSize = application.getTempMemory().getStaffList().size();

        for (int  i = 0; i < staffListSize; i ++) {
            new ExpertSystem().rules(application.getTempMemory().getStaffList().get(i), application.getTempMemory());
        }

        //  save to the report
        new ReportInterface().generateStaffReport(application, getStaffWithSolvableJobs());
    }

    public void analyseDepartment() {
        System.out.println("Analysing department...");

        int deparmentListSize = application.getTempMemory().getDepartmentList().size();

        for (int i = 0; i < deparmentListSize; i++) {
            new ExpertSystem().rules(application.getTempMemory().getDepartmentList().get(i), application.getTempMemory());
        }

        //save to the report
        new ReportInterface().generateDepartmentReport(application);
    }

    public void getJobProblemTypeTotals() {

        int technicalErrorCount = 0;
        int trainingCount = 0;

        //  Cycle through the list of jobs
        int jobListSize = application.getTempMemory().getJobList().size();
        for (int i = 0; i < jobListSize; i++) {
            if (application.getTempMemory().getJobList().get(i).getJobProblemType() == JobProblemType.TECHNICAL_ERROR) {
                technicalErrorCount++;
            }
            if (application.getTempMemory().getJobList().get(i).getJobProblemType() == JobProblemType.TRAINING) {
                trainingCount++;
            }
        }

        application.getTempMemory().setTechnicalErrorCount(technicalErrorCount);
        application.getTempMemory().setTraningCount(trainingCount);

        System.out.println("Tech error count: " + technicalErrorCount);
        System.out.println("Traning error count: " + trainingCount);

    }

    public JobProblemType getMostCommonJobProblemType() {
        if (application.getTempMemory().getTraningCount() > application.getTempMemory().getTechnicalErrorCount()) {
            return JobProblemType.TRAINING;
        }
        else return JobProblemType.TECHNICAL_ERROR;
    }

    private Staff getStaffWithSolvableJobs() {

        int count = 0;
        int hCount = 0;
        Staff staff = new Staff();

        for (int i = 0; i < application.getTempMemory().getStaffList().size(); i++ ) {

            int jobListSize = application.getTempMemory().getStaffList().get(i).getJobs().size();
            for (int j = 0; j < jobListSize; j++){
                if (application.getTempMemory().getStaffList().get(i).getJobs().get(j).getJobProblemType()
                        == JobProblemType.TRAINING) {
                    count ++;
                    if (count > hCount){
                        hCount = count;
                        staff = application.getTempMemory().getStaffList().get(i);
                    }
                }
            }
        }

        return staff;
    }

    public Department findDeptWithMostCalls() {
        TempMemory tempMemory = application.getTempMemory();
        Department department = new Department();
        int callCount = 0;
        int hCallCount = 0;

        for (int i = 0; i < tempMemory.getDepartmentList().size(); i++) {
            for (int j = 0; j < tempMemory.getDepartmentList().get(i).getStaffArray().size(); j++) {
                int staffCallCount = tempMemory.getDepartmentList().get(i).getStaffArray().get(j).getJobCount();
                callCount = callCount + staffCallCount;
            }
            if (callCount > hCallCount) {
                hCallCount = callCount;
                department = tempMemory.getDepartmentList().get(i);
            }
        }
        return department;
    }


}
