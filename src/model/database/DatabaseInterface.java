package model.database;

import java.sql.*;

/**
 * Created by Daniel on 09/04/2016.
 *
 * This class was created as proof of concept. It shows that the program can be extended to be used on a database.
 * However, a database was will not be used.
 */
public class DatabaseInterface {

    //  Database driver name & database URL
    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://localhost/databasetest";

    //  Database credentials
    static final String USER = "username";
    static final String PASS = "password";

    private Connection conn = null;
    private Statement stmt = null;

    public void connect() {
        try {

            //  Register JDBC driver
            Class.forName("com.mysql.jdbc.Driver");

            //  Open a connection
            System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL,USER,PASS);
            System.out.println("Connected");

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void retrieveDataFromStaff() {
        System.out.println("Creating statement...");

        try {
            stmt = conn.createStatement();

            String sql;;
            sql = "SELECT " + "staff_id, deparment_id, staff_name, call_amount"
                    + " FROM staff" ;
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                //  Retrieve by column name
                int s_id = rs.getInt("staff_id");
                int d_id = rs.getInt("department_id");
                int c_amount = rs.getInt("call_amount");
                String s_name = rs.getString("staff_name");
            }

            rs.close();
            stmt.close();
            conn.close();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            {
                try {
                    if(stmt!=null) {stmt.close();}
                } catch(SQLException se) {
                }
                try{
                    if(conn!=null) conn.close();
                }catch(SQLException se){
                    se.printStackTrace();
                }//end finally try
            }
        }
    }

    public void retrieveDataFromDepartment() {
        System.out.println("Creating statement...");

        try {
            stmt = conn.createStatement();

            String sql;;
            sql = "SELECT " + "department_id, department_name, department_calls, staff_id"
                    + " FROM department" ;
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                //  Retrieve by column name
                int d_id = rs.getInt("department_id");
                int s_id = rs.getInt("staff_id");
                int d_calls = rs.getInt("department_Calls");
                String d_name = rs.getString("department_name");
            }

            rs.close();
            stmt.close();
            conn.close();

            System.out.println("Retreieved info");
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            {
                try {
                    if(stmt!=null) {stmt.close();}
                } catch(SQLException se) {
                }
                try{
                    if(conn!=null) conn.close();
                }catch(SQLException se){
                    se.printStackTrace();
                }//end finally try
            }
        }
    }

    public void retrieveDataFromJobs() {
        System.out.println("Creating statement...");

        try {
            stmt = conn.createStatement();

            String sql;;
            sql = "SELECT " + "job_id, deparment_id, helpdesk_user_id," +
                    " caller_id, time_spent, completed, item_id, job_name, editor_name," +
                    "job_section"
                    + " FROM jobs" ;
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                //  Retrieve by column name
                int j_id = rs.getInt("job_id");
                int hd_id = rs.getInt("helpdesk_user_id");
                int c_id = rs.getInt("caller_id");
                int t_spent = rs.getInt("time_spent");
                int i_id = rs.getInt("item_id");
                String j_name = rs.getString("job_name");
                String e_name = rs.getString("editor_name");
                String j_section = rs.getString("job_section");
                String comp = rs.getString("completed");
            }

            rs.close();
            stmt.close();
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            {
                try {
                    if(stmt!=null) {stmt.close();}
                } catch(SQLException se) {
                }
                try{
                    if(conn!=null) conn.close();
                }catch(SQLException se){
                    se.printStackTrace();
                }//end finally try
            }
        }
    }

    public void retrieveDataFromJobStatus() {
        System.out.println("Creating statement...");

        try {
            stmt = conn.createStatement();

            String sql;;
            sql = "SELECT " + "job_outstanding, job_completed, dept_id, job_id" +
                    "staff_id"
                    + " FROM jobstatus" ;
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                //  Retrieve by column name
                int j_outstanding = rs.getInt("job_outstanding");
                int job_completed = rs.getInt("job_completed");
                int dept_id = rs.getInt("dept_id");
                int job_id = rs.getInt("job_id");
            }

            rs.close();
            stmt.close();
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            {
                try {
                    if(stmt!=null) {stmt.close();}
                } catch(SQLException se) {
                }
                try{
                    if(conn!=null) conn.close();
                }catch(SQLException se){
                    se.printStackTrace();
                }//end finally try
            }
        }
    }

    public void retrieveDataFromProduct() {
        System.out.println("Creating statement...");

        try {
            stmt = conn.createStatement();

            String sql;;
            sql = "SELECT " + "product_id, product_name, dept_id"
                    + " FROM product" ;
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                //  Retrieve by column name
                int product_id = rs.getInt("product_id");
                int product_name = rs.getInt("product_name");
                int dept_id = rs.getInt("dept_id");
            }

            rs.close();
            stmt.close();
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            {
                try {
                    if(stmt!=null) {stmt.close();}
                } catch(SQLException se) {
                }
                try{
                    if(conn!=null) conn.close();
                }catch(SQLException se){
                    se.printStackTrace();
                }//end finally try
            }
        }

    }

}
