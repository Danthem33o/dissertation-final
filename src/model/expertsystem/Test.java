package model.expertsystem;

import model.data.Jobs;
import model.database.DatabaseInterface;

import java.util.ArrayList;

/**
 * Created by Daniel on 15/04/2016.
 *
 * This class is used to test the expert system. It would not be needed in the final product, but is used to
 * for the expert system in demonstration.
 */
public class Test {

    private ArrayList<Jobs> jobs = new ArrayList<>();
    private ArrayList<Jobs> completedJobs = new ArrayList<>();
    private ArrayList<Jobs> uncopletedJobs = new ArrayList<>();

    private int compCount;
    private int uncompCount;

    //   Get the data to be filtered
    public void getData() {

        //   Filter the jobs
        for (int i = 0; i < jobs.size(); i++) {
            if (jobs.get(i).isCompleted()) completedJobs.add(jobs.get(i));
            else uncopletedJobs.add(uncopletedJobs.get(i));
        }

        //   apply the data to the rules
        if (!jobs.isEmpty()) {
            for (Jobs j: jobs) {

                if (j.isCompleted()) compCount++;
                else if (!j.isCompleted()) uncompCount++;

               // inferenceEngine.jobRules(j, jobs);

            }
        } else
            System.out.println("Job lists are empty");
    }

}
