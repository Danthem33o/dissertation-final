package model.expertsystem;

import com.sun.org.apache.bcel.internal.generic.IF_ACMPEQ;
import model.data.Department;
import model.data.Jobs;
import model.data.Staff;
import model.data.enums.DepartmentType;
import model.data.enums.JobProblemType;
import model.data.enums.JobSectionType;
import model.workingmemory.ConnectedList;
import model.workingmemory.TempMemory;

import java.util.ArrayList;

/**
 * Created by Daniel on 06/04/2016.
 *
 * This class is a combination of the Inference engine and the knowledge base. The rules are both
 * stored and executed in this class.
 *
 * These rules are a short sample of what a typical expert system would have. It shows proof of concept and
 * can be extended further. The lack of rules are due to a lack of knowledge in the field. These rules can
 * become far more extensive if an expert in the field was available.
 */
public class ExpertSystem {

    TempMemory tempMemory;

    /**
     * List of three rules that determines which rule sets to fire.
     * There are a list of three rule sets to fire: Job, Department
     * and staff.
     *
     * Checks to see if the object passing through is one of the three
     * main data object types.
     *
     * Looks for object of either staff, department or job.
     * @param object Object passed determines which set of rules are fired.
     *               The object is can be one of three type: jobs, staff
     *               and department.
     */
    public void rules(Object object, TempMemory tempMemory) {

        this.tempMemory = tempMemory;

        try {

            if (object instanceof Jobs) jobRules( ((Jobs)object) );
            if (object instanceof Staff) staffRules( ((Staff)object) );
            if (object instanceof Department) departmentRules( ((Department) object) );

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Incorrect object type passed.");
        }
    }

    /**
     * List of rules for jobs. If the antecedent is matched, then
     * the rule is fired.
     * @param job Data to check the antecedent against
     *            if matched, the rule can be fired.
     */
    public void jobRules(Jobs job) {

        //   Rule #1
        if (!job.isCompleted()) {
            System.out.println("\nJob: '" + job.getJobName() + "' matched Rule #1" + "\n");
            job.setInNeedSolution(true);
        }

        //   Rule #2
        if (job.isInNeedSolution()) {
            System.out.println("Firing rule #2\n" + "Searching list..." + "\n");
            for (Jobs j : tempMemory.getCompletedJobs()) {
                //   if not the same job, check if in the same section and job type
                if (j.getJob_id() != job.getJob_id())
                    if (j.getJobSection().equals(job.getJobSection()))
                        if (j.getJobProblemType().equals(job.getJobProblemType())) {
                            tempMemory.getJobsolutions().add(new ConnectedList(job, j));
                            System.out.println("Job solutions have been found and added to the list");
                            for (ConnectedList c : tempMemory.getJobsolutions()) {
                                System.out.println("Job Solution: " + c.getSolution().getSolution() + "\n"
                                        + "Job with problem: " + c.getNeedSolution().getJob_id() + "\n"
                                        + "found from: " + c.getSolution().getJob_id() + "\n");
                                System.out.println("Reasoning: " + "Job is not completed, job needed solution, job with same problem domain found" +
                                        " solution given.");
                            }
                            return; //   End if statement is matched
                        }
            }
            //   If no statement is executed, then no solution found
            System.out.println("No solution found.");
        }

    }

    /**
     * List of rules for department.
     * @param department Data to check the antecedent against
     *                   if matched, then the rule can be fired.
     */
    public void departmentRules(Department department) {
        //RULE #1
        for (int i = 0; i < department.getStaffArray().size(); i++)
            if (!department.getStaffArray().get(i).getJobs().get(i).isCompleted()) {
                department.getStaffArray().get(i).getJobs().get(i).setInNeedSolution(true);
            }

        //RULE #2
        if (department.getStaffTotalCalls() > tempMemory.getMostCallsDepartment().getStaffTotalCalls()) {
            tempMemory.setMostCallsDepartment(department);
        }

        //RULE #3
        for (int i = 0; i < department.getStaffArray().size(); i++)
            if (department.getStaffArray().get(i).getJobCount() > tempMemory.getHighestCaller().getJobCount()) {
                tempMemory.setHighestCaller(department.getStaffFromArray(i));
            }

        //RULE#4
        for (int i = 0; i < department.getStaffArray().size(); i++)
            for (int j = 0; j < department.getStaffArray().get(i).getJobs().size(); j++)
                if (department.getStaffArray().get(i).getJobs().get(j).isInNeedSolution()) {
                    jobRules(department.getStaffArray().get(i).getJobs().get(j));
                }
    }

    /**
     * List of rules for staff.
     * @param staff Data to check the antecedent against.
     *              If matched, the rule can be fired.
     */
    public void staffRules(Staff staff) {
        //RULE #1
        if (staff.getJobCount() > tempMemory.getCallCount()) {
            tempMemory.setHighestCaller(staff);
        }

        //RULE #2
        for (int i = 0; i < staff.getJobs().size(); i++) {
            if (staff.getJobs().get(i).getJobProblemType() == JobProblemType.TRAINING) {
                tempMemory.getStaffNeedTraining().add(new ConnectedList(staff, staff.getJobs().get(i).getJobSection()));
            }
        }

    }

}
