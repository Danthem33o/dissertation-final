package screens.popup;

import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import main.Main;

import java.io.IOException;

/**
 * Created by Daniel on 08/03/2016.
 *
 * Loads an FXML file and sets a height and width that was passed in the argument.
 *
 */
public class PopUp extends Stage {

    private Parent root;
    private Scene scene;

    private String fxml;

    private Main application;


    /**
     * Constructor class takes the width, height, title, fxml and main class as its arguments.
     * Also sets the modality of the popup within the constructor to stop interaction of the main
     * stage when the popup is active. It also always sets it on top of the main view.
     * @param width width of the popup.
     * @param height height of the popup.
     * @param title title of the popup.
     * @param fxml fxml file location and name of the popup.
     * @param main main application reference to allow state changes to occur.
     * @throws IOException
     * @throws InterruptedException
     */
    public PopUp(int width, int height, String title, String fxml, Main main) throws IOException, InterruptedException {
        //   Set the properties
        setTitle(title);
        setWidth(width);
        setHeight(height);
        this.fxml = fxml;
        application = main;

        //   load the fxml to the root node
        root = FXMLLoader.load(getClass().getResource(fxml));

        scene = new Scene(root, width, height);

        setTitle(title);
        this.setResizable(false);
        initModality(Modality.WINDOW_MODAL);
        initOwner(application.getStage().getScene().getWindow());
        setScene(scene);
        setAlwaysOnTop(true);
        show();
    }

    //   blank constructor
    public PopUp() {
    }

    /**
     * Sets the modality of the popup. Stops it from moving.
     */
    public void setUnclick() {
        initModality(Modality.WINDOW_MODAL);
        initOwner(application.getStage().getScene().getWindow());
    }

    public String getFxml() {
        return fxml;
    }

    public void setFxml(String fxml) {
        this.fxml = fxml;
    }

    public Main getApplication() {
        return application;
    }
}
