package screens.controllers;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import main.Main;

import java.awt.*;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Daniel on 07/03/2016.
 *
 */
public class DocumentSelectionScreenController implements Initializable {

    private Main application;

    private Desktop desktop = Desktop.getDesktop();

    @FXML
    private Label openLbl;
    @FXML
    private Label recentLbl;
    @FXML
    private Label newLbl;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }

    public void setApp(Main main) {
        application = main;
    }


    public void openDocument() throws IOException {
        application.openDocument();
    }

    public void recentDocument() throws IOException, InterruptedException {
        application.goToRecentPopup();
    }

    public void newDocument() throws IOException, InterruptedException {
        //Set new document details - Load new popup.
        application.goToNewDocumentPopup();
    }

    public void openTutorials() throws IOException, InterruptedException {
        application.goToTutorialScreen();
    }



}
