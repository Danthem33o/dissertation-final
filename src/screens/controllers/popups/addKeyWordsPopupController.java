package screens.controllers.popups;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import model.workingmemory.TempMemory;
import screens.popup.PopUp;

/**
 * Created by Daniel on 14/04/2016.
 *
 * Handles all of the events of the add key words popup.
 *
 */
public class addKeyWordsPopupController {

    @FXML
    TextField keyWordsTextField;
    @FXML
    Button keyWordsAddButton;
    @FXML
    ListView<String> keyWordsListView;

    ObservableList<String> keyWords = FXCollections.observableArrayList();

    public void populateList() {
        //   get the list from local storage
        System.out.println("Populating list...");
    }

    public void addWords(ActionEvent event) {
        if (!keyWordsTextField.getText().isEmpty()) {
            String word = keyWordsTextField.getText();
            if (!keyWords.contains(word)) {
                keyWords.add(word);
                keyWordsListView.setItems(keyWords);

                Node source = (Node)  event.getSource();

                PopUp stage  = (PopUp) source.getScene().getWindow();
                TempMemory tempMemory = stage.getApplication().getTempMemory();

                tempMemory.addKeyWordsToArray(word);

                System.out.println("Words in array: ");
                for (int i=0; i < tempMemory.getKeyWordsArray().size(); i ++) {
                    System.out.println(tempMemory.getKeyWordsArray().get(i));
                }

                System.out.println("Key word " + word);
            } else {
                System.out.println("Word already added");
            }
            keyWordsTextField.setText("");
        } else {
            System.out.println("Empty field, nothing added.");
        }
    }

}
