package screens.controllers.popups;

import javafx.event.ActionEvent;
import javafx.scene.Node;
import screens.popup.PopUp;

import java.io.IOException;

/**
 * Created by Daniel on 08/03/2016.
 *
 */
public class SettingsScreenController {

    public void close(ActionEvent event) throws IOException {
        Node source = (Node)  event.getSource();

        PopUp stage  = (PopUp) source.getScene().getWindow();
        stage.getApplication().goToMainScreen();
        stage.close();
    }

}
