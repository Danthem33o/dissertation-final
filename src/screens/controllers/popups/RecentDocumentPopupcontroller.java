package screens.controllers.popups;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.ListView;
import javafx.stage.WindowEvent;
import model.workingmemory.TempMemory;
import screens.popup.PopUp;

import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Daniel on 10/03/2016.
 *
 * Handles all of the events of the recent document popup.
 */
public class RecentDocumentPopupcontroller {

    @FXML
    private ListView recentListView;

    private ObservableList<String> list = FXCollections.observableArrayList("Suzie");

    public void close(ActionEvent event) throws IOException {
        Node source = (Node)  event.getSource();
        PopUp stage  = (PopUp) source.getScene().getWindow();
        stage.close();
    }


    public void open(ActionEvent event) throws IOException {
        if (recentListView.getItems().isEmpty()) {
            System.out.println("No items in list, cannot open file.");
        } else if (recentListView.getSelectionModel().isEmpty()) {
            System.out.println("No file has been chosen");
        } else {
            Node  source = (Node)  event.getSource();
            PopUp stage  = (PopUp) source.getScene().getWindow();

            recentListView.setItems(list);
            System.out.println("Found: " + recentListView.getSelectionModel().getSelectedItem());

            //stage.getApplication().openChosenDocument(recentListView.getSelectionModel().getSelectedItem().toString());

            stage.close();
        }
    }

    public void tes() {
        for (int i = 0; i < recentListView.getItems().size(); i++) {
            recentListView.getItems().get(i);
        }
    }

    public void getRecentDocuments(ActionEvent event) {
        Node source = (Node)  event.getSource();
        PopUp stage  = (PopUp) source.getScene().getWindow();

        ArrayList docArray = stage.getApplication().getTempMemory().getRecentDocuments();

        for (int i = 0; i < docArray.size(); i ++) {
           list.add(String.valueOf(docArray.get(i)));
        }
    }



}
