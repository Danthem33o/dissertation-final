package screens.controllers.popups;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import main.Main;
import model.RecentFilesBuffer;
import screens.popup.PopUp;

import java.io.IOException;

/**
 * Created by Daniel on 09/03/2016.
 *
 * Handles all of the events of the new document popup.
 *
 */
public class NewDocumentPopupController {

    private Main application;
    //private PopUp popUp;

    @FXML
    private Label warningLabel;
    @FXML
    private ImageView warningIcon;
    @FXML
    private TextField docNameTxtField;
    @FXML
    private TextArea descriptionTxtArea;
    @FXML
    private Button saveBtn;
    @FXML
    private Button cancelBtn;
    @FXML
    private Label ErrorText;

    private String docName;



    public void setApp(Main main) {
        application = main;
    }


    public void close(ActionEvent event) throws IOException {
        Node  source = (Node)  event.getSource();

        PopUp stage  = (PopUp) source.getScene().getWindow();
        stage.close();
    }

    public void create(ActionEvent event) throws IOException {
        ErrorText = new Label();
        ErrorText.setText("hi hi"); //  testing..

        if (docNameTxtField.getText().isEmpty() || docNameTxtField.getText().equals(" ")) {
            System.out.println("No text entered");
        }
        else {
            //Get name
            docName = docNameTxtField.getText();

            RecentFilesBuffer buffer = new RecentFilesBuffer();
            buffer.addRecentDocument(docName);

            Node source = (Node) event.getSource();

            PopUp stage = (PopUp) source.getScene().getWindow();
            stage.getApplication().setTitle(docName);
            stage.getApplication().goToMainScreen();

            stage.close();
        }
    }

    public boolean hasError() {
        return true;
    }



}
