package screens.controllers.popups;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import main.Main;
import model.Testing;
import screens.popup.PopUp;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Daniel on 08/03/2016.
 *
 * Tutorial screen controller. It handles all of the events in the tutorial popup.
 */
public class TutorialScreenController implements Initializable {

    private Main application;

    @FXML
    private Button CloseButton;
    @FXML
    private Label descriptionBodyText;
    @FXML
    private Label imageLabel1;
    @FXML
    private Label imageLabel2;
    @FXML
    private ImageView descriptionBodyImage;



    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public void setApp(Main main) {
        application = main;
    }

    /* Events */

    /**
     * Action event method.
     * Closes the popup and goes to the previous screen.
     *
     * @param event
     * @throws IOException
     */
    public void goToPreviousScreen(ActionEvent event) throws IOException {
        Node source = (Node)  event.getSource();

        PopUp stage  = (PopUp) source.getScene().getWindow();
        stage.close();
    }

    public void setBodyText() {

    }

    public void imageClicked() {

    }

    /**
     * Action event method.
     * Triggered when the first image is clicked.
     *
     * Replaces the text in the body and the image.
     */
    public void image1Clicked() {
        descriptionBodyText.setText(Testing.tutorialBodyText);
        descriptionBodyImage.setImage(new Image("/resources/img/tutorialPopupScreen/testImage.png"));
    }

    /**
     * Action event method.
     * Triggered when the second image is clicked.
     *
     * Replaces the text in the body and the image.
     */
    public void image2Clicked() {
        descriptionBodyText.setText(Testing.tutorialBodyText2);
        descriptionBodyImage.setImage(new Image("/resources/img/tutorialPopupScreen/testImage2.png"));
    }

}
