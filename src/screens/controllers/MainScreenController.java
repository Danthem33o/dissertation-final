package screens.controllers;


import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.paint.Paint;
import javafx.scene.text.*;
import main.Main;
import model.Analyse;
import model.report.Report;
import utils.DOM;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;


/**
 * Created by Daniel on 07/03/2016.
 *
 * The main screen controller class handles all of the events that are triggered by the user.
 * It has direct communication from the FXML class, the user and the model classes.
 *
 * It has a reference of the main application to share and update the program's state.
 *
 */
public class MainScreenController implements Initializable {

    private Main application;

    @FXML
    TextFlow reportBox;
    @FXML
    Button reportButton;

    @FXML
    TabPane tabPane;
    @FXML
    Tab searchTab;

    //   Radio buttons
    @FXML
    RadioButton jobRadioButton;
    @FXML
    RadioButton staffRadioButton;
    @FXML
    RadioButton departmentRadioButton;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }

    public void setApp(Main main) {
        application = main;
    }

    /**
     * Sets the initial field settings of all the elements.
     * To be called before the view is loaded up.
     */
    public void setFieldSettings() {

        ToggleGroup toggleGroup = new ToggleGroup();
        jobRadioButton.setToggleGroup(toggleGroup);
        staffRadioButton.setToggleGroup(toggleGroup);
        departmentRadioButton.setToggleGroup(toggleGroup);

    }

    /**
     * sets the report when one is opened up. To be used before the view
     * loads up.
     *
     * Disables the report button. Also sets the font, the font weight and
     * the font size.
     *
     * @param report The report that is being opened up.
     */
    public void setReport(Report report) {
        //blank out report button
        reportButton.setDisable(true);

        int count = 0; //section no. identifier
        for (int i = 0; i < report.getSections().size(); i++) {
            count ++;
            Text body = new Text(report.getSections().get(i).getBodyText() + "\n\n");
            Text header = new Text(count + "   " + report.getSections().get(i).getHeadingText() + "\n");
            Text subHeader = new Text("\n" + + count+ "." + count + "   " +  report.getSections().get(i).getSubheadingText()+ "\n");

            body.setFont(javafx.scene.text.Font.font("Verdana", 14));
            header.setFont(javafx.scene.text.Font.font("Verdana", FontWeight.BOLD, 28));
            subHeader.setFont(javafx.scene.text.Font.font("Verdana", FontWeight.BOLD, FontPosture.ITALIC, 18));

            body.setFill(Paint.valueOf("#fffbff"));
            header.setFill(Paint.valueOf("#fffbff"));
            subHeader.setFill(Paint.valueOf("#fffbff"));

            reportBox.getChildren().addAll(header, subHeader, body);
        }
    }

    public void setNewReport(Report report) {
        int count = 0; //section no. identifier
        for (int i = 0; i < report.getSections().size(); i++) {
            count ++;
            Text body = new Text(report.getSections().get(i).getBodyText());
            Text header = new Text(count + "   " + report.getSections().get(i).getHeadingText() + "\n");
            Text subHeader = new Text("\n" + + count+ "." + count + "   " +  report.getSections().get(i).getSubheadingText()+ "\n");

            body.setFont(javafx.scene.text.Font.font("Verdana", 14));
            header.setFont(javafx.scene.text.Font.font("Verdana", FontWeight.BOLD, 28));
            subHeader.setFont(javafx.scene.text.Font.font("Verdana", FontWeight.BOLD, FontPosture.ITALIC, 18));

            body.setFill(Paint.valueOf("#fffbff"));
            header.setFill(Paint.valueOf("#fffbff"));
            subHeader.setFill(Paint.valueOf("#fffbff"));


            reportBox.getChildren().addAll(header, subHeader, body);
        }
    }

    /**
     * Clears the report set in the text flow.
     */
    public void clearReport() {
        reportBox.getChildren().clear();
    }

    /* Event Handlers */

    /**
     * Action event method.
     * Called when the new document menu item is chosen.
     *
     * Opens up the new document popup by accessing it through the main class.
     *
     * @throws IOException
     * @throws InterruptedException
     */
    public void goToNew() throws IOException, InterruptedException {
        application.goToNewDocumentPopup();
    }

    /**
     * Action event method.
     * Called when the open menu item is chosen.
     *
     * Opens up the file chooser to find a new file through the main class.
     *
     * @throws IOException
     */
    public void goToOpen() throws IOException {
        application.openDocument();
    }

    /**
     * Action event method.
     * Called when the recent document menu item is chosen.
     *
     * Opens up the recent document popup through the main class.
     *
     * @throws IOException
     * @throws InterruptedException
     */
    public void goToRecent() throws IOException, InterruptedException {
        application.goToRecentPopup();
    }

    /**
     * Action event method.
     * Called when the tutorials menu item is chosen.
     *
     * Opens up the tutorials popup through the main class.
     *
     * @throws IOException
     * @throws InterruptedException
     */
    public void goToTutorials() throws IOException, InterruptedException {
        application.goToTutorialScreen();
    }

    /**
     * Action event method.
     * Called when the close menu item is chosen.
     *
     * Closes the application through the exit() method in the main class.
     */
    public void close() {
        application.exit();
    }

    /**
     * Action event method.
     * Called when the save menu item is chosen.
     *
     * Saved the report if it is not empty. Does so by creating a new XML
     * document through the DOM.
     *
     * @throws IOException
     */
    public void save() throws IOException, TransformerException, ParserConfigurationException {
        if (reportBox.getChildren().isEmpty()) {
            System.out.println("No report open, document not saved");
        } else {
            application.getTempMemory().getRecentDocuments().add(application.getTitle());
            new DOM().createDocument(application.getTitle(), application.getTempMemory().getReport());

            System.out.println("Document added to recent documents:");
            for (int i = 0; i < application.getTempMemory().getRecentDocuments().size(); i++) {
                System.out.println(application.getTempMemory().getRecentDocuments().get(i));
            }
        }
        //send saved notification to the user
    }

    /**
     * Action event method.
     * Called when the add key words menu item is chosen.
     *
     * Opens the add key words popup through the main class.
     *
     * @throws IOException
     * @throws InterruptedException
     */
    public void addKeyWords() throws IOException, InterruptedException {
        application.openAddWords();
    }


    /* Report button event handler */

    /**
     * Action event method.
     * Called when the report button is clicked by the user.
     * Firstly finds the search parameters set by the user, then the analytic class
     * is ran, then lastly it is presented to the user.
     *
     * @throws Exception Exception thrown if fails.
     */
    public void generateReport() throws Exception {
        System.out.println("Generating Report");

        //   Find the parameters selected
        try {
            findParameters();

            System.out.println("Report generating...");
            clearReport();
            setNewReport(application.getTempMemory().getReport());
        } catch (Exception e) {
            System.out.println("No search Parameters found ");
            e.printStackTrace();
        }

    }

    /**
     * Finds the search parameters selected by the user. The test
     * data is then generated.
     */
    public void findParameters() {
        application.getTempMemory().generateTests();
        Analyse analyse = new Analyse(application);

        System.out.println("Finding parameters...");

       try {
            if (jobRadioButton.isSelected()) {
                System.out.println("Jobs selected");
                analyse.analyseJobs();
            }
            else if (staffRadioButton.isSelected()) {
                System.out.println("Staff selected");
                analyse.analyseStaff();
            }
            else if (departmentRadioButton.isSelected()) {
                System.out.println("Department selected");
                analyse.analyseDepartment();
            } else if (!departmentRadioButton.isSelected()&& !staffRadioButton.isSelected() &&
                    !jobRadioButton.isSelected()) {
                System.out.println("No param selected");
            }
        } catch (Exception e) {
            System.out.println("No parameters found");
        }
    }


}
