package main;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.fxml.JavaFXBuilderFactory;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import model.RecentFilesBuffer;
import model.report.Report;
import model.workingmemory.TempMemory;
import screens.controllers.DocumentSelectionScreenController;
import screens.controllers.MainScreenController;
import screens.popup.PopUp;
import utils.DOM;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The main class is the first class that is ran when the application is launched.
 * The main classed is passed throughout the program and used in all of the controllers
 * to share the program's state.
 *
 * It holds all of the methods to switch views and create popups.
 * The temporary memory is also first created here. It is used throughout the program.
 */
public class Main extends Application {

    //--------------------------------------------------------------------------------------------------------
    //-------------------                           VARIABLES

    private Stage stage;
    private static int height, width;
    private static String title = "Dissertation"; // Default text.

    private String filePath;
    private Report report;
    private boolean reportSet = false; //   Used to determine if the main screen should load a report
                                       //   upon entering the main screen.

    private TempMemory tempMemory;     //   Create the temporary memory to access.

    private PopUp popUp; //  Popup to be used when creating new popups.

    //--------------------------------------------------------------------------------------------------------
    //-------------------                           METHODS

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        setStageProperties(primaryStage);
        goToDocumentSelectionScreen();
        primaryStage.show();
    }

    /**
     * Sets the main stage's properties.
     * Properties set are: title and resizable.
     *
     * @param primaryStage The main stage.
     */
    public void setStageProperties(Stage primaryStage) {
        stage = primaryStage;
        stage.setTitle(title);
        stage.setResizable(false);
    }

    /**
     * Action event method. If the exit option is chosen this closes
     * the main stage and the application.c
     */
    public void exit() {
        stage.close();
    }

    /**
     * Used to swap the scene content (i.e. screen/view) to fxml file specified.
     * Used to swap the main scene content only. All popup views are defined when
     * creating a popup.
     *
     * @param fxml The fxml file name which the existing view will be swapped with.
     *             The absolute file path is already specified within the method.
     */
    public Initializable replaceSceneContent(String fxml) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        InputStream in = Main.class.getResourceAsStream("/screens/fxml/" + fxml);

        loader.setBuilderFactory(new JavaFXBuilderFactory());
        loader.setLocation(Main.class.getResource(fxml));

        AnchorPane page;
        page = (AnchorPane) loader.load(in);
        in.close();

        tempMemory = new TempMemory();

        stage.setScene(new Scene(page, width, height));
        stage.sizeToScene();

        return (Initializable) loader.getController();
    }

    /**
     * Sets the view to the document selections screen view. The view's
     * height, width, title and controller are set within the method.
     *
     * @throws IOException throws exception if fail.
     */
    public void goToDocumentSelectionScreen() throws IOException {
        try {
            //Change height for the document selector
            setHeight(400);
            setWidth(800);
            stage.setTitle("Welcome");
            DocumentSelectionScreenController controller = (DocumentSelectionScreenController) replaceSceneContent("documentSelection.fxml");
            controller.setApp(this);
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Sets the view to the main screen view. The view's height, width,
     * title and controller are set within the method.
     *
     * If the report has already been set, then it loads the report before
     * opening the main screen.
     *
     * @throws IOException Throws exception if fail.
     */
    public void goToMainScreen() throws IOException {
        try {
            setHeight(700);
            setWidth(1200);
            stage.setTitle(title);
            MainScreenController controller = (MainScreenController) replaceSceneContent("mainScreen.fxml");
            controller.setFieldSettings();
            if (reportSet) controller.setReport(report);
            else controller.clearReport();
            controller.setApp(this);
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


    //PopUps

    /**
     * Creates a new popup stage (view) for the tutorial popup screen that appears above the main stage.
     * The absolute file path of the fxml file is required for the popup.
     *
     * @throws IOException
     * @throws InterruptedException
     */
    public void goToTutorialScreen() throws IOException, InterruptedException {
        popUp = new PopUp(975, 704, "Tutorial", "/screens/fxml/popups/tutorialScreen.fxml", this);
    }

    /**
     * Create a new popup stage (view) for the new document popup screen that appears above the main stage.
     * The absolute file path of the fxml file is required.
     *
     * @throws IOException
     * @throws InterruptedException
     */
    public void goToNewDocumentPopup() throws IOException, InterruptedException {
        reportSet = false;
        popUp = new PopUp(615, 205, "Select Document", "/screens/fxml/popups/documentCreatePopUp.fxml", this);
    }

    /**
     * Creates a new popup stage (view) for the recent documents popup screen that appears above the main stage.
     * The absolute file path of the fxml file is required.
     *
     * @throws IOException
     * @throws InterruptedException
     */
    public void goToRecentPopup() throws IOException, InterruptedException {
        reportSet = true;
        popUp = new PopUp(615, 445, "Select Recent Document", "/screens/fxml/popups/recentPopupScreen.fxml", this);
    }

    public void goToAdd() {
    }

    public void goToSettingsScreen() throws IOException {
        try {
            stage.setTitle("Settings");
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Creates a new popup stage (view) for the add key words popup.
     *
     * @throws IOException
     * @throws InterruptedException
     */
    public void openAddWords() throws IOException, InterruptedException {
        popUp = new PopUp(615,445, "Add Key Words", "/screens/fxml/popups/addKeyWords.fxml", this);
    }

    /**
     * Opens a file chooser to allow the user to choose a file to open.
     * The file chooser is configured in the configureFileChooser() method.
     *
     * @throws IOException
     */
    public void openDocument() throws IOException {
        //Run file explorer
        FileChooser fileChooser = new FileChooser();
        configureFileChooser(fileChooser);

        File file = fileChooser.showOpenDialog(this.getStage());
        if (file != null) {
            openFile(file);
        }
    }

    /**
     * Configures the file chooser passed through.
     * Sets the title, initial directory and the file filters.
     * The file filters only allows XML file extensions to be opened.
     *
     * @param fileChooser the file chooser to set the configuration to.
     */
    public void configureFileChooser(final FileChooser fileChooser) {
        fileChooser.setTitle("Select file");
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Docs (*.xml)", "*.XML", "*.xml"));
        fileChooser.setInitialDirectory(new File("C:\\Users\\Daniel\\Documents\\BitBucket\\Projects\\Dissertation\\Dissertation Project\\samples"));

    }

    /**
     * Opens the file passed through.
     * File is read through the DOM by creating a new report and
     * passing through the file location.
     * Sets the report boolean to true if opens.
     *
     * @param file
     * @throws IOException
     */
    public void openFile(File file) throws IOException {
        //get the file path
        String location = file.getPath();
        System.out.println(location);

        setFilePath(location);

        try {
            Report report = new DOM().getDocument(location);
            setReportSet(true);
            setReport(report);
        } catch (Exception e) {
            System.out.println("File failed to open.");
        }

        goToMainScreen();
    }


    //-------------------------------------- GETTERS & SETTERS
    public Stage getStage() {
        return stage;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public static int getHeight() {
        return height;
    }

    public static void setHeight(int height) {
        Main.height = height;
    }

    public static int getWidth() {
        return width;
    }

    public static void setWidth(int width) {
        Main.width = width;
    }

    public static String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        Main.title = title;
    }

    public PopUp getPopUp() {
        return popUp;
    }

    public void setPopUp(PopUp popUp) {
        this.popUp = popUp;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public Report getReport() {
        return report;
    }

    public void setReport(Report report) {
        this.report = report;
    }

    public boolean isReportSet() {
        return reportSet;
    }

    public void setReportSet(boolean reportSet) {
        this.reportSet = reportSet;
    }

    public TempMemory getTempMemory() {
        return tempMemory;
    }

    public void setTempMemory(TempMemory tempMemory) {
        this.tempMemory = tempMemory;
    }

}