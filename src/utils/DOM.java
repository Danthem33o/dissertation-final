package utils;

import model.report.Report;
import model.report.ReportSection;
import model.workingmemory.TempMemory;
import org.w3c.dom.*;

import javax.xml.parsers.*;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;

/**
 * XML interface. Opens and creates XML documents.
 */
public class DOM {

    //private String fileName = "test.txt"; /* To change when recent implemented */
    Report report = new Report();

    /**
     * Opens the document passed through.
     *
     * Finds all of the nodes and then reads the values. Stores the values in a
     * report section.
     *
     * @param fileName File to be read.
     * @return The final report after all report sections have been added to it.
     */
    public Report getDocument(String fileName) {
        try {
            // get the file
            File inputFile = new File(fileName);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(inputFile);
            doc.getDocumentElement().normalize();

            //Find the root node... -- i.e. report
            String rootNode = doc.getDocumentElement().getNodeName();

            //get all nodes with...  -- i.e. the section element
            NodeList nList = doc.getElementsByTagName("section");

            //cycle through each section
            for (int temp = 0; temp < nList.getLength(); temp++) {
                //get the current node...
                Node nNode = nList.item(temp);

                //Node working on...
                String node = nNode.getNodeName();

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;

                    //Get the main title
                    String title =
                    eElement.getElementsByTagName("title").item(0).getTextContent();

                    //get the sub title
                    String subtitle =
                    eElement.getElementsByTagName("subtitle").item(0).getTextContent();

                    //get the body
                    String body =
                    eElement.getElementsByTagName("body").item(0).getTextContent();

                    //Add each section to the report
                    ReportSection section = new ReportSection(title, subtitle, body);
                    report.add(section);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Incorrect XML file format chosen");
        }
        return report;
    }

    public void createDocument(String fileName, Report report) throws ParserConfigurationException, TransformerException {
        try {
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

            //create root element
            Document doc = docBuilder.newDocument();
            Element rootElement = doc.createElement("report");
            doc.appendChild(rootElement);

            //section elements
            for (int i = 0; i < report.getSections().size(); i++) {
                //Create the section element
                Element section = doc.createElement("section");
                rootElement.appendChild(section);

                //get the title element from storage and add
                Element title = doc.createElement("title");
                String sTitle = report.getSections().get(i).getHeading().getText();
                title.appendChild(doc.createTextNode(sTitle));
                section.appendChild(title);

                //get subtitle and add
                Element subtitle = doc.createElement("subtitle");
                String sSub = report.getSections().get(i).getSubheadingText();
                subtitle.appendChild(doc.createTextNode(sSub));
                section.appendChild(subtitle);

                //get the body and add
                Element body = doc.createElement("body");
                String sBody = report.getSections().get(i).getBody().getText();
                body.appendChild(doc.createTextNode(sBody));
                section.appendChild(body);
            }

            // write the content into xml file
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File(
                    "C:\\Users\\Daniel\\Documents\\BitBucket\\Projects\\Dissertation\\Dissertation Project\\samples\\"
                            + fileName + ".xml"));

            transformer.transform(source, result);
            System.out.println("File saved!");
        } catch (Exception e) {
            System.out.println("Incorrect XML file format chosen");
        }
    }
}

